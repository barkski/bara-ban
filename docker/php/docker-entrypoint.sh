#!/bin/sh

if [ -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ];
then
    if [ -z "$HOST" ]; then
        HOST=`/sbin/ip route|awk '/default/ { print $3 }'`
    fi

    sed -i "s/xdebug\.client_host\=.*/xdebug\.client_host\=$HOST/g" /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    echo "${HOST} was set as xdebug.client_host"
fi

exec $@
