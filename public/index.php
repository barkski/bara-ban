<?php

use Barkski\BaraBan\Presentation\RequestHandler;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__) . '/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__) . '/.env');

$debug = isset($_COOKIE['debug']) || !empty($_SERVER['APP_DEBUG']);

if ($debug) {
    umask(0000);

    Debug::enable();
}

$response = (new RequestHandler())->handle(Request::createFromGlobals());

// fixme: hack for cors
$response->headers->set('Access-Control-Allow-Origin', '*');
$response->headers->set('Access-Control-Allow-Methods', '*');
$response->headers->set('Access-Control-Allow-Headers', '*');
$response->headers->set('Access-Control-Allow-Credentials', '*');

$response->send();
