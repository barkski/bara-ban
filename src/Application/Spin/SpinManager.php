<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Application\Spin;

use Barkski\BaraBan\Domain\Spin\SpinFactory;

final class SpinManager
{
    private SpinFactory $spinFactory;

    public function __construct(SpinFactory $spinFactory)
    {
        $this->spinFactory = $spinFactory;
    }

    public function spin(int $bet): SpinViewModel
    {
        $spin = $this->spinFactory->create($bet);

        return new SpinViewModel(
            $spin->getLines(),
            $spin->getReels(),
            $spin->getWinMeta()->getWin(),
            $spin->getWinMeta()->getTotalWin(),
            $spin->getWinMeta()->getWinSyms(),
        );
    }
}
