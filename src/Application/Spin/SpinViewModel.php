<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Application\Spin;

use Barkski\BaraBan\Domain\Spin\Line;
use JsonSerializable;

final class SpinViewModel implements JsonSerializable
{
    /**
     * @var Line[]
     */
    private array $lines;

    /**
     * @var int[][]
     */
    private array $reels;

    private int $win;
    private int $totalWin;
    private int $winSyms;

    public function __construct(array $lines, array $reels, int $win, int $totalWin, int $winSyms)
    {
        $this->lines    = $lines;
        $this->reels    = $reels;
        $this->win      = $win;
        $this->totalWin = $totalWin;
        $this->winSyms  = $winSyms;
    }

    public function jsonSerialize(): array
    {
        return [
            "action"     => "spin",
            "bet"        => 1,
            "lines"      => $this->lines,
            "reels"      => $this->reels,
            "win"        => $this->win,
            "totalWin"   => $this->totalWin,
            "winSyms"    => $this->winSyms,
            // fixme: what is it
            "nReels"     => [
                [
                    4,
                    11,
                    3,
                    5,
                    7,
                    10,
                ],
                [
                    10,
                    8,
                    3,
                    11,
                    6,
                    8,
                ],
                [
                    7,
                    2,
                    2,
                    3,
                    2,
                    8,
                ],
                [
                    7,
                    10,
                    8,
                    10,
                    3,
                    10,
                ],
                [
                    8,
                    10,
                    5,
                    7,
                    8,
                    9,
                ],
            ],
            "feature"    => "basic",
            "nextAction" => "spin",
            "balance"    => 666, // fixme: no info
        ];
    }

}
