<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Application\Slot;

final class SlotInitManager
{
    public function init(): InitResponse
    {
        return new InitResponse();
    }
}
