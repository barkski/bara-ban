<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Application\Slot;

use Barkski\BaraBan\Domain\Spin\PaytableProvider;
use Barkski\BaraBan\Domain\Spin\RoutesProvider;
use JsonSerializable;

final class InitResponse implements JsonSerializable
{
    public function jsonSerialize(): array
    {
        return [
            "action"     => "init",
            "homeURL"    => "javascript:history.back()",
            "language"   => "en",
            "currency"   => "MXN",
            "gameId"     => "lions_pride",
            "gameName"   => "Lion's Pride",
            "gameConfig" => [
                "quickSpin" => [
                    "maxWatcherCount" => 3,
                ],
                "bigWin"    => [
                    "bigMultiplier"   => 50,
                    "giantMultiplier" => 100,
                ],
                "trtp"      => 94.16,
                "spinDelay" => 0,
                "autoPlay"  => [
                    "counts" => [
                        10,
                        25,
                        50,
                        100,
                        150,
                        300,
                        -1,
                    ],
                ],
            ],
            "balance"    => 998809,
            "bets"       => [
                1,
                2,
                5,
                10,
            ],
            "bet"        => 1,
            "betCoins"   => 40,
            "lines"      => RoutesProvider::fetch(),
            "paytable"   => PaytableProvider::getPaytable(),
            "reels"      => [
                [
                    11,
                    10,
                    3,
                    10,
                ],
                [
                    5,
                    10,
                    10,
                    8,
                ],
                [
                    9,
                    7,
                    9,
                    8,
                ],
                [
                    2,
                    2,
                    6,
                    6,
                ],
                [
                    6,
                    7,
                    6,
                    2,
                ],
            ],
            "nReels"     => [
                [
                    11,
                    10,
                    3,
                    10,
                    6,
                    9,
                ],
                [
                    8,
                    3,
                    5,
                    10,
                    5,
                    4,
                ],
                [
                    3,
                    11,
                    5,
                    10,
                    5,
                    10,
                ],
                [
                    8,
                    8,
                    7,
                    9,
                    4,
                    11,
                ],
                [
                    10,
                    10,
                    10,
                    10,
                    8,
                    6,
                ],
            ],
            "feature"    => "basic",
            "nextAction" => "spin",
        ];
    }
}
