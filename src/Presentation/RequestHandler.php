<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Presentation;

use Barkski\BaraBan\Application\Slot\SlotInitManager;
use Barkski\BaraBan\Application\Spin\SpinManager;
use Barkski\BaraBan\Domain\Spin\SpinFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

use Throwable;

use function json_decode;

final class RequestHandler
{
    public function handle(Request $request): Response
    {
        try {
            return $this->process($request);
        } catch (HttpExceptionInterface $exception) {
            return new JsonResponse(
                $this->formatErrorMessage($exception->getMessage() ?: 'Bad request'),
                $exception->getStatusCode()
            );
        } catch (Throwable $exception) {
            return new JsonResponse(
                $this->formatErrorMessage($exception->getMessage() ?: 'Unexpected error'), 500
            );
        }
    }

    private function process(Request $request): JsonResponse
    {
        $controller = new Controller(
            new SlotInitManager(),
            new SpinManager(new SpinFactory()),
        );

        if ($request->getMethod() !== 'POST') {
            return new JsonResponse([], Response::HTTP_NO_CONTENT);
        }

        $request = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $action = $request['action'];

        switch ($action) {
            case 'init':
                $response = $controller->init();
                break;
            case 'spin':
                $response = $controller->spin((int)$request['bet']);
                break;
            default:
                throw new BadRequestHttpException('Unknown action');
        }

        return $response;
    }

    private function formatErrorMessage(string $message): array
    {
        return ['message' => $message];
    }
}
