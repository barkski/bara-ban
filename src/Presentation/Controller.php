<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Presentation;

use Barkski\BaraBan\Application\Slot\SlotInitManager;
use Barkski\BaraBan\Application\Spin\SpinManager;
use Symfony\Component\HttpFoundation\JsonResponse;

final class Controller
{
    private SlotInitManager $initManager;
    private SpinManager     $spinManager;

    public function __construct(SlotInitManager $initManager, SpinManager $spinManager)
    {
        $this->initManager = $initManager;
        $this->spinManager = $spinManager;
    }

    public function init(): JsonResponse
    {
        return new JsonResponse($this->initManager->init());
    }

    public function spin(int $bet): JsonResponse
    {
        return new JsonResponse($this->spinManager->spin($bet));
    }
}
