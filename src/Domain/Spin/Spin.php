<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

final class Spin
{
    /**
     * @var int[][]
     */
    private array $reels;

    /**
     * @var Line[]
     */
    private array $lines;

    private WinMeta $winMeta;

    /**
     * @param int[][] $reels
     * @param Line[]  $lines
     */
    public function __construct(array $reels, array $lines, WinMeta $winMeta)
    {
        $this->reels   = $reels;
        $this->lines   = $lines;
        $this->winMeta = $winMeta;
    }

    /**
     * @return int[][]
     */
    public function getReels(): array
    {
        return $this->reels;
    }

    /**
     * @return Line[]
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    public function getWinMeta(): WinMeta
    {
        return $this->winMeta;
    }
}
