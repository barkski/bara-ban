<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

final class SpinFactory
{
    private ReelsFactory  $reelsFactory;
    private LineProcessor $lineProcessor;
    private WinCalculator $winCalculator;

    public function __construct()
    {
        $this->reelsFactory = new ReelsFactory();
        $this->lineProcessor = new LineProcessor(RoutesProvider::fetch());
        $this->winCalculator = new WinCalculator();
    }

    public function create(int $bet): Spin
    {
        $reels = $this->reelsFactory->roll();

        $lines = $this->lineProcessor->process($reels, $bet);

        $winMeta = $this->winCalculator->getWinMeta($lines);

        return new Spin($reels, $lines, $winMeta);
    }
}
