<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

final class MultiplierProvider
{
    private array $indexedPayTable;

    public function __construct()
    {
        foreach (PaytableProvider::getPaytable() as $payRule) {
            $sym = $payRule['sym'];
            $n = $payRule['n'];

            $this->indexedPayTable[$sym][$n] = $payRule;
        }
    }

    public function getMultiplier(int $symbol, int $count): int
    {
        return $this->indexedPayTable[$symbol][$count]['mul'] ?? 0;
    }
}
