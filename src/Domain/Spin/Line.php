<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

use JsonSerializable;

final class Line implements JsonSerializable # fixme: create view model for domain object and implement jsonSerializable there
{
    private int $line;
    private int $sym;
    private int $n;

    /**
     * @var int[]
     */
    private array $pos;

    private int $win;
    private int $mul;

    /**
     * @var mixed[]
     */
    private array $comb;

    private string $dir;

    /**
     * @param mixed[] $comb
     * @param int[]   $pos
     */
    public function __construct(int $line, int $sym, int $n, array $pos, int $win, int $mul, array $comb, string $dir)
    {
        $this->line = $line;
        $this->sym  = $sym;
        $this->n    = $n;
        $this->pos  = $pos;
        $this->win  = $win;
        $this->mul  = $mul;
        $this->comb = $comb;
        $this->dir  = $dir;
    }

    public function getLine(): int
    {
        return $this->line;
    }

    public function getSym(): int
    {
        return $this->sym;
    }

    public function getN(): int
    {
        return $this->n;
    }

    /**
     * @return int[]
     */
    public function getPos(): array
    {
        return $this->pos;
    }

    public function getWin(): int
    {
        return $this->win;
    }

    public function getMul(): int
    {
        return $this->mul;
    }

    /**
     * @return mixed[]
     */
    public function getComb(): array
    {
        return $this->comb;
    }

    public function getDir(): string
    {
        return $this->dir;
    }

    public function jsonSerialize(): array
    {
        return [
            'line' => $this->getLine(),
            'n'    => $this->getN(),
            'pos'  => $this->getPos(),
            'sym'  => $this->getSym(),
            'win'  => $this->win,
            'mul'  => $this->mul,
            'dir'  => $this->dir,
            'comb' => $this->comb,
        ];
    }

}
