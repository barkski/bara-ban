<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

final class WinMeta
{
    private int $win;
    private int $totalWin; // fixme: what is the difference with `win`?
    private int $winSyms;

    public function __construct(int $win, int $totalWin, int $winSyms)
    {
        $this->win      = $win;
        $this->totalWin = $totalWin;
        $this->winSyms  = $winSyms;
    }

    public function getWin(): int
    {
        return $this->win;
    }

    public function getTotalWin(): int
    {
        return $this->totalWin;
    }

    public function getWinSyms(): int
    {
        return $this->winSyms;
    }
}
