<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

final class LineProcessor
{
    private MultiplierProvider $multiplierProvider;

    /**
     * @var int[][]
     */
    private array $routes;

    /**
     * @param int[][] $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
        $this->multiplierProvider = new MultiplierProvider();
    }

    /**
     * @param int[][] $reels
     *
     * @return Line[]
     */
    public function process(array $reels, int $bet): array
    {
        $winningLines = [];

        foreach ($this->routes as $routeIndex => $route) {
            $currentSymbol = null;
            $counter       = 0;
            $winningRoute = [-1, -1, -1, -1, -1];

            foreach ($route as $index => $position) {
                if (null === $currentSymbol) {
                    $currentSymbol = $reels[$index][$position];
                }

                $newSymbol = $reels[$index][$position];

                if (PaytableProvider::WILD_SYMBOL === $currentSymbol) {
                    /**
                     * According to "PRIORITISATION" chapter we need to decide what symbol to use for leading wilds.
                     * From game's paytable we can conclude that we always pay more for bigger sequences.
                     * Example:
                     *  `W W -1 -1 -1` -> the biggest multiplier is `5` for `2 2 -1 -1 -1`
                     *  `W W 11 -1 -1` -> going to `11 11 11 -1 -1` -> multiplier is `4`.
                     *
                     * So we can just rewrite current symbol anytime we get a new non-wild symbol.
                     */
                    $currentSymbol = $newSymbol;
                }

                if ($currentSymbol === $newSymbol) {
                    $winningRoute[$index] = $position;

                    $counter++;
                } else {
                    break;
                }
            }

            if (PaytableProvider::WILD_SYMBOL === $currentSymbol) {
                // if we have full route of wilds just transform it to the most expensive one.
                $currentSymbol = PaytableProvider::MOST_EXPENSIVE_SYMBOL;
            }

            $multiplier = $this->multiplierProvider->getMultiplier($currentSymbol, $counter);

            if (0 !== $multiplier) {
                $winningLines[] = new Line(
                    $routeIndex,
                    $currentSymbol,
                    $counter,
                    $winningRoute,
                    $multiplier * $bet,
                    $multiplier,
                    [], // fixme: what is it
                    'left'
                );
            }
        }

        return $winningLines;
    }
}
