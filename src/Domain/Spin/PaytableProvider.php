<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

final class PaytableProvider
{
    public const WILD_SYMBOL = 1;
    public const MOST_EXPENSIVE_SYMBOL = 2;

    public static function getPaytable(): array
    {
        return [
            [
                "sym"       => 0,
                "n"         => 3,
                "mul"       => 4,
                "type"      => "scatter",
                "freespins" => 10,
            ],
            [
                "sym"  => 2,
                "n"    => 5,
                "mul"  => 1000,
                "type" => "betline",
            ],
            [
                "sym"  => 3,
                "n"    => 5,
                "mul"  => 500,
                "type" => "betline",
            ],
            [
                "sym"  => 4,
                "n"    => 5,
                "mul"  => 500,
                "type" => "betline",
            ],
            [
                "sym"  => 5,
                "n"    => 5,
                "mul"  => 400,
                "type" => "betline",
            ],
            [
                "sym"  => 6,
                "n"    => 5,
                "mul"  => 300,
                "type" => "betline",
            ],
            [
                "sym"  => 7,
                "n"    => 5,
                "mul"  => 300,
                "type" => "betline",
            ],
            [
                "sym"  => 2,
                "n"    => 4,
                "mul"  => 200,
                "type" => "betline",
            ],
            [
                "sym"  => 8,
                "n"    => 5,
                "mul"  => 200,
                "type" => "betline",
            ],
            [
                "sym"  => 9,
                "n"    => 5,
                "mul"  => 200,
                "type" => "betline",
            ],
            [
                "sym"  => 3,
                "n"    => 4,
                "mul"  => 150,
                "type" => "betline",
            ],
            [
                "sym"  => 4,
                "n"    => 4,
                "mul"  => 150,
                "type" => "betline",
            ],
            [
                "sym"  => 5,
                "n"    => 4,
                "mul"  => 100,
                "type" => "betline",
            ],
            [
                "sym"  => 10,
                "n"    => 5,
                "mul"  => 100,
                "type" => "betline",
            ],
            [
                "sym"  => 11,
                "n"    => 5,
                "mul"  => 100,
                "type" => "betline",
            ],
            [
                "sym"  => 6,
                "n"    => 4,
                "mul"  => 80,
                "type" => "betline",
            ],
            [
                "sym"  => 7,
                "n"    => 4,
                "mul"  => 80,
                "type" => "betline",
            ],
            [
                "sym"  => 8,
                "n"    => 4,
                "mul"  => 50,
                "type" => "betline",
            ],
            [
                "sym"  => 9,
                "n"    => 4,
                "mul"  => 50,
                "type" => "betline",
            ],
            [
                "sym"  => 2,
                "n"    => 3,
                "mul"  => 40,
                "type" => "betline",
            ],
            [
                "sym"  => 3,
                "n"    => 3,
                "mul"  => 40,
                "type" => "betline",
            ],
            [
                "sym"  => 4,
                "n"    => 3,
                "mul"  => 40,
                "type" => "betline",
            ],
            [
                "sym"  => 10,
                "n"    => 4,
                "mul"  => 30,
                "type" => "betline",
            ],
            [
                "sym"  => 11,
                "n"    => 4,
                "mul"  => 30,
                "type" => "betline",
            ],
            [
                "sym"  => 5,
                "n"    => 3,
                "mul"  => 20,
                "type" => "betline",
            ],
            [
                "sym"  => 6,
                "n"    => 3,
                "mul"  => 20,
                "type" => "betline",
            ],
            [
                "sym"  => 7,
                "n"    => 3,
                "mul"  => 20,
                "type" => "betline",
            ],
            [
                "sym"  => 8,
                "n"    => 3,
                "mul"  => 10,
                "type" => "betline",
            ],
            [
                "sym"  => 9,
                "n"    => 3,
                "mul"  => 10,
                "type" => "betline",
            ],
            [
                "sym"  => 10,
                "n"    => 3,
                "mul"  => 5,
                "type" => "betline",
            ],
            [
                "sym"  => 11,
                "n"    => 3,
                "mul"  => 5,
                "type" => "betline",
            ],
            [
                "sym"  => 2,
                "n"    => 2,
                "mul"  => 4,
                "type" => "betline",
            ],
            [
                "sym"  => 3,
                "n"    => 2,
                "mul"  => 2,
                "type" => "betline",
            ],
            [
                "sym"  => 4,
                "n"    => 2,
                "mul"  => 2,
                "type" => "betline",
            ],
        ];
    }
}
