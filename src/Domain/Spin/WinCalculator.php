<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

final class WinCalculator
{
    /**
     * @param Line[] $lines
     */
    public function getWinMeta(array $lines): WinMeta
    {
        $winMoney = 0;
        $winSymbolsIndexed = [];

        foreach ($lines as $line) {
            $winMoney += $line->getWin();

            foreach ($line->getPos() as $index => $value) {
                if ($value === -1) {
                    break;
                }

                $winSymbolsIndexed[$index . ' ' . $value] = true;
            }

        }

        return new WinMeta($winMoney, $winMoney, count($winSymbolsIndexed));
    }

}
