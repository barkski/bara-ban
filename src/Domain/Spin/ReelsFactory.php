<?php

declare(strict_types=1);

namespace Barkski\BaraBan\Domain\Spin;

use function array_map;
use function random_int;
use function range;

final class ReelsFactory
{
    /**
     * @return int[][]
     */
    public function roll(): array
    {
        return array_map(fn() => array_map(
            fn() => $this->getRandomSym(),
            range(1, 4)
        ), range(1, 5));
    }

    private function getRandomSym(): int
    {
        return random_int(1, 11);
    }
}
